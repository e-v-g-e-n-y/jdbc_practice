package jdbc_practice;

import jdbc_practice.model.Group;
import jdbc_practice.service.DBService;

import java.util.HashMap;
import java.util.Map;


// особенность: хранит только те группы, которые есть в БД
public class GroupFactory {
    private Map<Integer, Group> groups = new HashMap<>();
    private DBService dbService;

    public GroupFactory(DBService dbService) {
        this.dbService = dbService;
    }

    public Group getGroup(Integer id) {
        Log.addFactoriesLog("GroupFactory.getGroup:Получение информации по группе#" + id);
        Group result = groups.get(id);
        if ((result == null) && (dbService != null)){
            Log.addFactoriesLog("GroupFactory.getGroup:Поиск в БД группы#" + id);
            result = dbService.getGroup(id);
            if (result != null) {
                groups.put(id, result);
                Log.addFactoriesLog("GroupFactory.getGroup:result1=" + result);
                return result;
            }
        }
        Log.addFactoriesLog("GroupFactory.getGroup:result2=" + result);
        return result;
    }
    public void setGroup(Group group) {
        Log.addFactoriesLog("GroupFactory.setGroup:start with group=" + group);
        if (group == null) {
            Log.addError("GroupFactory.setGroup:Попытка сохранить пустую группу:" + group);
            return;
        }
        Group existingGroup = getGroup(group.getId());
        if (existingGroup != null) {
            // группа с таким id есть, надо сравнить с группой-параметром и при необходимости записать в БД
            Log.addFactoriesLog("GroupFactory.setGroup: dictionaryObj=" + existingGroup);
            if (existingGroup.equals(group)) {
                Log.addFactoriesLog("GroupFactory.setGroup: groups are equal(" + group + "," + existingGroup);
                return;
            }
        }
        // такой группы нет в БД
        Log.addFactoriesLog("GroupFactory.setGroup:adding group");
        dbService.setGroup(group);
        Log.addFactoriesLog("GroupFactory.setGroup:finish with group=" + group);
    }
}
