package jdbc_practice.model;

import java.util.Objects;

public class Group {
    private int id;
    private String name;
    private int groupGraduation;

    public Group() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroupGraduation() {
        return groupGraduation;
    }

    public void setGroupGraduation(int groupGraduation) {
        this.groupGraduation = groupGraduation;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", groupGraduation=" + groupGraduation +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return id == group.id &&
                groupGraduation == group.groupGraduation &&
                Objects.equals(name, group.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, groupGraduation);
    }
}
