package jdbc_practice.model;

public class Study {
    private int id;
    private int idGroup;
    private String subject;
    private String room;

    public Study() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "Study{" +
                "id=" + id +
                ", idGroup=" + idGroup +
                ", subject='" + subject + '\'' +
                ", room='" + room + '\'' +
                '}';
    }
}
