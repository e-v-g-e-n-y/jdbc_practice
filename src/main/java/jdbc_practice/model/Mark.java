package jdbc_practice.model;

public class Mark {
    private int id;
    private int idStudent;
    private int idStudy;
    private int grade;

    public Mark() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getIdStudy() {
        return idStudy;
    }

    public void setIdStudy(int idStudy) {
        this.idStudy = idStudy;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Mark{" +
                "id=" + id +
                ", idStudent=" + idStudent +
                ", idStudy=" + idStudy +
                ", grade=" + grade +
                '}';
    }
}
