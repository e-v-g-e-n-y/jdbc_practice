package jdbc_practice;

import jdbc_practice.da.*;
import jdbc_practice.da.connection.ConnectionManagerJdbc;
import jdbc_practice.model.Mark;
import jdbc_practice.model.Student;
import jdbc_practice.model.Study;
import jdbc_practice.service.DBService;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ConnectionManagerJdbc connectionManagerJdbc = new ConnectionManagerJdbc(1, "jdbc:postgresql://localhost:5432/DBStudents",
                "postgres", "sa");
        DBService svc = new DBService(
                new StudentDaoJdbcImpl(connectionManagerJdbc),
                new GroupDaoJbclImpl(connectionManagerJdbc),
                new CityDaoJdbcImpl(connectionManagerJdbc),
                new StudyDaoJdbcImpl(connectionManagerJdbc),
                new MarkDaoJdbcImpl(connectionManagerJdbc)
        );
        for (int i = 1; i < 4; i++) {
            Student student = svc.getStudent(i);
            System.out.println(student.toString());
        }

        Student st = svc.getStudent(4);
        st.setFirstName("testFName");
        st.setMiddleName("testMName");
        st.setLastName("testLName");
        svc.setStudent(st);

//        Group g = new Group();
//        g.setName("debug");
//        g.setGroupGraduation(2222);
//        svc.setGroup(g);

//        Group g = new Group();
//        g.setId(4);
//        svc.removeGroup(g);


//        City c = new City();
//        c.setName("debug_city");
//        svc.setCity(c);

//        City c2 = new City();
//        c2.setId(10);
//        svc.removeCity(c2);

        // список студентов по группе
        int groupId = 1;
        List<Student> studentList = svc.getStudentsByGroup(groupId);
        System.out.println("Состав группы#" + groupId);
        System.out.println(Arrays.toString(studentList.toArray()));;

        // получение списка всех студентов
        List<Student> studentList2 = svc.getStudents();
        System.out.println("Список всех студентов:");
        System.out.println(Arrays.toString(studentList2.toArray()));;


        // получение занятия по идентификатору
        Study study1 = svc.getStudyById(1);
        System.out.println("study1=" + study1.toString());

        // получение занятия по номеру группы
        List<Study> studyList2 = svc.getStudyListByGroupId(1);
        System.out.println("studyList2=" + Arrays.toString(studyList2.toArray()));

        // получение занятия по номеру группы
        List<Study> studyList3 = svc.getStudyList();
        System.out.println("studyList3=" + Arrays.toString(studyList3.toArray()));

//        // добавление занятий
//        for (int i = 1; i < 5; i++) {
//            Study study4 = new Study();
//            study4.setIdGroup(3);
//            study4.setSubject("Занятие№" + i + " группа 3");
//            study4.setRoom("3-" + i);
//            svc.setStudy(study4);
//        }

//        // удаление занятия
//        Study study5 = new Study();
//        study5.setId(9);
//        svc.removeStudy(study5);

        // получение оценки по её id в таблице
        Mark mark1 = svc.getMarkById(1);
        System.out.println(mark1.toString());

       // получение списка оценок по студенту (по id)
        List<Mark> markList1 = svc.getMarkListByStudentId(1);
        System.out.println("Список оценок студента#1=" + Arrays.toString(markList1.toArray()));

        // получение оценок по занятию (по id занятия)
        List<Mark> markList2 = svc.getMarkListByStudyId(1);
        System.out.println("Список оценок студента#1=" + Arrays.toString(markList2.toArray()));

//        // сохранение оценки
//        Mark mark2 = new Mark();
//        mark2.setIdStudy(1);
//        mark2.setIdStudent(4);
//        mark2.setGrade(2);
//        svc.setMark(mark2);

        // удаление оценки
        Mark mark3 = new Mark();
        mark3.setId(8);
        svc.removeMark(mark3);
    }
}
