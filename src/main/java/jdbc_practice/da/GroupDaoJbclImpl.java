package jdbc_practice.da;

import jdbc_practice.da.connection.ConnectionManagerJdbc;
import jdbc_practice.da.dto.GroupDto;
import jdbc_practice.da.mapper.GroupMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupDaoJbclImpl implements GroupDao {
    private ConnectionManagerJdbc connectionManager;

    public GroupDaoJbclImpl(ConnectionManagerJdbc connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public GroupDto getGroupDtoById(Integer id) {
        GroupDto result = null;
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from groups where id = ?")) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        result = GroupMapper.mapResultSetToDto(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void setGroup(GroupDto groupDto) {
        String sqlText = "INSERT INTO groups (id, name, group_graduation)\n" +
                "VALUES\n" +
                "(\n" +
                "  COALESCE(NULLIF(?, -1),nextval('groups_id_seq')),\n" +
                "  ?,\n" +
                "  ?\n" +
                ")\n" +
                "ON CONFLICT (id)\n" +
                "   DO  UPDATE\n" +
                "     SET name = EXCLUDED.name, group_graduation = EXCLUDED.group_graduation";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, groupDto.getId());
                preparedStatement.setString(2, groupDto.getName());
                preparedStatement.setInt(3, groupDto.getGroup_graduation());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeGroup(GroupDto groupDto) {
        String sqlText = "DELETE FROM groups WHERE id = ?";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, groupDto.getId());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
