package jdbc_practice.da;

import jdbc_practice.da.dto.CityDto;

public interface CityDao {
    public CityDto getCityDtoById(Integer id);
    public void setCity(CityDto cityDto);
    public void removeCity(CityDto cityDto);
}
