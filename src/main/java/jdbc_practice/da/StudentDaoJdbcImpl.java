package jdbc_practice.da;

import jdbc_practice.da.connection.ConnectionManagerJdbc;
import jdbc_practice.da.dto.StudentDto;
import jdbc_practice.da.mapper.StudentMapper;
import jdbc_practice.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoJdbcImpl implements StudentDao {
    private ConnectionManagerJdbc connectionManager;
    private List<StudentDto> getStudentDtoListWithStatement(PreparedStatement preparedStatement){
        List<StudentDto> studentDtoList = new ArrayList<>();
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                studentDtoList.add(StudentMapper.mapResultSetToDto(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentDtoList;
    }

    public StudentDaoJdbcImpl(ConnectionManagerJdbc connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public StudentDto getStudentById(Integer id) {
        StudentDto result = new StudentDto();
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from student where id = ?")) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        result = StudentMapper.mapResultSetToDto(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void setStudent(StudentDto studentDto) {
        String sqlText =
                "INSERT INTO student(id, first_name, middle_name, last_name, date_of_birth, group_id, city_id)\n" +
                "VALUES\n" +
                "(\n" +
                "  COALESCE(NULLIF(?, -1),nextval('student_id_seq')),\n" + // 1 id
                "?," + // 2 first_name
                "?," + // 3 middle_name
                "?," + // 4 last_name
                "?," + // 5 date_of_birth
                "?," + // 6 group_id
                "?" + // 7 city_id
                ")\n" +
                "ON CONFLICT (id)\n" +
                "   DO  UPDATE\n" +
                "     SET first_name = EXCLUDED.first_name, middle_name = EXCLUDED.middle_name, last_name = EXCLUDED.last_name," +
                "          date_of_birth = EXCLUDED.date_of_birth, group_id = EXCLUDED.group_id, city_id = EXCLUDED.city_id";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, studentDto.getId());
                preparedStatement.setString(2, studentDto.getFirst_name());
                preparedStatement.setString(3, studentDto.getMiddle_name());
                preparedStatement.setString(4, studentDto.getLast_name());
                preparedStatement.setDate(5, new java.sql.Date(studentDto.getDate_of_birth().getTime()));
                preparedStatement.setInt(6, studentDto.getGroup_id());
                preparedStatement.setInt(7, studentDto.getCity_id());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeStudent(StudentDto studentDto) {
        String sqlText = "DELETE FROM student WHERE id = ?";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, studentDto.getId());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<StudentDto> getStudentsByGroupId(int id) {
        List<StudentDto> studentDtoList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from student where group_id = ?")) {
                preparedStatement.setInt(1, id);
                return getStudentDtoListWithStatement(preparedStatement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentDtoList;
    }

    @Override
    public List<StudentDto> getStudents() {
        List<StudentDto> studentDtoList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from student");
            return getStudentDtoListWithStatement(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentDtoList;
    }
}