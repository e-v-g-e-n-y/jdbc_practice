package jdbc_practice.da;

import jdbc_practice.da.dto.GroupDto;
import jdbc_practice.model.Group;

public interface GroupDao {
    public GroupDto getGroupDtoById(Integer id);
    public void setGroup(GroupDto groupDto);
    public void removeGroup(GroupDto groupDto);
}
