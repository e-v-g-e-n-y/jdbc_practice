package jdbc_practice.da;

import jdbc_practice.da.connection.ConnectionManagerJdbc;
import jdbc_practice.da.dto.CityDto;
import jdbc_practice.da.mapper.CityMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CityDaoJdbcImpl implements CityDao {
    private ConnectionManagerJdbc connectionManager;

    public CityDaoJdbcImpl(ConnectionManagerJdbc connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public CityDto getCityDtoById(Integer id) {
        CityDto result = null;
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from city where id = ?")) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        result = CityMapper.mapResultSetToDto(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void setCity(CityDto cityDto) {
        String sqlText = "INSERT INTO city (id, name)\n" +
                "VALUES\n" +
                "(\n" +
                "  COALESCE(NULLIF(?, -1),nextval('cities_id_seq')),\n" +
                "  ?\n" +
                ")\n" +
                "ON CONFLICT (id)\n" +
                "   DO  UPDATE\n" +
                "     SET name = EXCLUDED.name";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, cityDto.getId());
                preparedStatement.setString(2, cityDto.getName());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeCity(CityDto cityDto) {
        String sqlText = "DELETE FROM city WHERE id = ?";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, cityDto.getId());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
