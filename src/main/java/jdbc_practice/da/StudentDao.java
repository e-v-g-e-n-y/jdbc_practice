package jdbc_practice.da;

import jdbc_practice.da.dto.StudentDto;
import jdbc_practice.model.Student;

import java.util.List;

public interface StudentDao {
        public StudentDto getStudentById(Integer id);
        public void setStudent(StudentDto studentDto);
        public void removeStudent(StudentDto studentDto);
        public List<StudentDto> getStudentsByGroupId(int id);
        public List<StudentDto> getStudents();
}
