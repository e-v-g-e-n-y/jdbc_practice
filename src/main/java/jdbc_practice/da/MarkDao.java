package jdbc_practice.da;

import jdbc_practice.da.dto.MarkDto;

import java.util.List;

public interface MarkDao {
    public MarkDto getMarkDtoById(Integer id);
    public void setMark(MarkDto markDto);
    public void removeMark(MarkDto markDto);
    public List<MarkDto> getMarkListByStudyId(int idStudy);
    public List<MarkDto> getMarkListByStudentId(int idStudent);
}
