package jdbc_practice.da.mapper;

import jdbc_practice.da.dto.CityDto;
import jdbc_practice.model.City;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityMapper {
    public static CityDto mapResultSetToDto(ResultSet resultSet) throws SQLException {
        CityDto studentDto = new CityDto();
            studentDto.setId(resultSet.getInt("id"));
            studentDto.setName(resultSet.getString("name"));
        return studentDto;
    }
    public static City MappDtoToCity(CityDto cityDto) {
        if (cityDto == null) { return  null;}
        City result = new City();
            result.setId(cityDto.getId());
            result.setName(cityDto.getName());
        return  result;
    }
    public static CityDto MappCityToDto(City city) {
        CityDto result = new CityDto();
        result.setId(city.getId());
        result.setName(city.getName());
        return result;
    }
}
