package jdbc_practice.da.mapper;

import jdbc_practice.CityFactory;
import jdbc_practice.GroupFactory;
import jdbc_practice.da.dto.StudentDto;
import jdbc_practice.model.Student;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentMapper {
    public static StudentDto mapResultSetToDto(ResultSet resultSet) throws SQLException {
        StudentDto studentDto = new StudentDto();

            studentDto.setId(resultSet.getInt("id"));
            studentDto.setFirst_name(resultSet.getString("first_name"));
            studentDto.setMiddle_name(resultSet.getString("middle_name"));
            studentDto.setLast_name(resultSet.getString("last_name"));
            studentDto.setDate_of_birth(resultSet.getDate("date_of_birth"));
            studentDto.setGroup_id(resultSet.getInt("group_id"));
            studentDto.setCity_id(resultSet.getInt("city_id"));
        return studentDto;
    }
    public static Student MappDtoToStudent(StudentDto studentDto, GroupFactory groupFactory, CityFactory cityFactory)  {
    Student student = new Student();
        student.setId(studentDto.getId());
        student.setFirstName(studentDto.getFirst_name());
        student.setMiddleName(studentDto.getMiddle_name());
        student.setLastName(studentDto.getLast_name());
        student.setDateOfBirth(studentDto.getDate_of_birth());
        student.setGroup(groupFactory.getGroup(studentDto.getGroup_id()));
        student.setCity(cityFactory.getCity(studentDto.getCity_id()));
    return student;
    }
    public static StudentDto MappStudentToDto(Student student) {
        StudentDto result = new StudentDto();
        result.setId(student.getId());
        result.setFirst_name(student.getFirstName());
        result.setMiddle_name(student.getMiddleName());
        result.setLast_name(student.getLastName());
        result.setDate_of_birth(student.getDateOfBirth());
        result.setGroup_id(student.getGroup().getId());
        result.setCity_id(student.getCity().getId());
        return result;
    }
}
