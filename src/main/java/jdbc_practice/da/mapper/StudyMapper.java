package jdbc_practice.da.mapper;

import jdbc_practice.da.dto.StudyDto;
import jdbc_practice.model.Study;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudyMapper {
    public static StudyDto mapResultSetToDto(ResultSet resultSet) throws SQLException {
        StudyDto studyDto = new StudyDto();
        studyDto.setId(resultSet.getInt("id"));
        studyDto.setIdGroup(resultSet.getInt("id_group"));
        studyDto.setSubject(resultSet.getString("subject"));
        studyDto.setRoom(resultSet.getString("room"));
        return studyDto;
    }
    public static Study MappDtoToStudy(StudyDto studyDto) {
        Study study = new Study();
        study.setId(studyDto.getId());
        study.setIdGroup(studyDto.getIdGroup());
        study.setSubject(studyDto.getSubject());
        study.setRoom(studyDto.getRoom());
        return study;
    }
    public static  StudyDto MappStudyToDto(Study study) {
        StudyDto studyDto = new StudyDto();
        studyDto.setId(study.getId());
        studyDto.setIdGroup(study.getIdGroup());
        studyDto.setSubject(study.getSubject());
        studyDto.setRoom(study.getRoom());
        return studyDto;
    }
}
