package jdbc_practice.da.mapper;

import jdbc_practice.da.dto.MarkDto;
import jdbc_practice.model.Mark;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MarkMapper {
    public static MarkDto mapResultSetToDto(ResultSet resultSet) throws SQLException {
        MarkDto markDto = new MarkDto();
        markDto.setId(resultSet.getInt("id"));
        markDto.setIdStudy(resultSet.getInt("id_study"));
        markDto.setIdStudent(resultSet.getInt("id_student"));
        markDto.setGrade(resultSet.getInt("grade"));
        return markDto;
    }
    public static Mark MappDtoToMark(MarkDto markDto) {
        Mark mark = new Mark();
        mark.setId(markDto.getId());
        mark.setIdStudy(markDto.getIdStudy());
        mark.setIdStudent(markDto.getIdStudent());
        mark.setGrade(markDto.getGrade());
        return mark;
    }
    public static MarkDto MappMarkToDto(Mark mark) {
        MarkDto markDto = new MarkDto();
        markDto.setId(mark.getId());
        markDto.setIdStudy(mark.getIdStudy());
        markDto.setIdStudent(mark.getIdStudent());
        markDto.setGrade(mark.getGrade());
        return markDto;
    }
}
