package jdbc_practice.da.mapper;

import jdbc_practice.da.dto.GroupDto;
import jdbc_practice.model.Group;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupMapper {
    public static GroupDto mapResultSetToDto(ResultSet resultSet) throws SQLException {
        GroupDto groupDto = new GroupDto();
            groupDto.setId(resultSet.getInt("id"));
            groupDto.setName(resultSet.getString("name"));
            groupDto.setGroup_graduation(resultSet.getInt("group_graduation"));
        return groupDto;
    }
    public static Group MappDtoToGroup(GroupDto groupDto) {
        Group result = new Group();
        result.setId(groupDto.getId());
        result.setName(groupDto.getName());
        result.setGroupGraduation(groupDto.getGroup_graduation());
        return  result;
    }
    public static  GroupDto MappGroupToDto(Group group) {
        GroupDto result = new GroupDto();
        result.setId(group.getId());
        result.setName(group.getName());
        result.setGroup_graduation(group.getGroupGraduation());
        return result;
    }
}
