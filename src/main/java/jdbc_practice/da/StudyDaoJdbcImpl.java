package jdbc_practice.da;

import jdbc_practice.da.connection.ConnectionManagerJdbc;
import jdbc_practice.da.dto.GroupDto;
import jdbc_practice.da.dto.StudyDto;
import jdbc_practice.da.mapper.GroupMapper;
import jdbc_practice.da.mapper.StudyMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudyDaoJdbcImpl implements StudyDao {
    private ConnectionManagerJdbc connectionManager;

    private List<StudyDto> getStudyDtoListWithStatement(PreparedStatement preparedStatement){
        List<StudyDto> studyDtoList = new ArrayList<>();
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                studyDtoList.add(StudyMapper.mapResultSetToDto(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studyDtoList;
    }

    public StudyDaoJdbcImpl(ConnectionManagerJdbc connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public StudyDto getStudyDtoById(Integer id) {
        StudyDto result = null;
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from study where id = ?")) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        result = StudyMapper.mapResultSetToDto(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void setStudy(StudyDto studyDto) {
        String sqlText =
                "INSERT INTO study(id, id_group, subject, room)\n" +
                        "VALUES\n" +
                        "(\n" +
                        "  COALESCE(NULLIF(?, -1),nextval('study_id_seq')),\n" + // 1 id
                        "?," + // 2 id_group
                        "?," + // 3 subject
                        "?" + // 4 room
                        ")\n" +
                        "ON CONFLICT (id)\n" +
                        "   DO  UPDATE\n" +
                        "     SET id_group = EXCLUDED.id_group, subject = EXCLUDED.subject, room = EXCLUDED.room";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, studyDto.getId());
                preparedStatement.setInt(2, studyDto.getIdGroup());
                preparedStatement.setString(3, studyDto.getSubject());
                preparedStatement.setString(4, studyDto.getRoom());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeStudy(StudyDto studyDto) {
        String sqlText = "DELETE FROM study WHERE id = ?";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, studyDto.getId());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<StudyDto> getStudyListByGroupId(int idGroup) {
        List<StudyDto> studentDtoList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from study WHERE id_group = ?")) {
                preparedStatement.setInt(1, idGroup);
                return getStudyDtoListWithStatement(preparedStatement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentDtoList;
    }

    @Override
    public List<StudyDto> getStudyList() {
        List<StudyDto> studentDtoList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from study");
            return getStudyDtoListWithStatement(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentDtoList;
    }
}
