package jdbc_practice.da;

import jdbc_practice.da.connection.ConnectionManagerJdbc;
import jdbc_practice.da.dto.MarkDto;
import jdbc_practice.da.mapper.MarkMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MarkDaoJdbcImpl implements MarkDao {
    private ConnectionManagerJdbc connectionManager;

    public MarkDaoJdbcImpl(ConnectionManagerJdbc connectionManager) {
        this.connectionManager = connectionManager;
    }

    private List<MarkDto> getMarkDtoListWithStatement(PreparedStatement preparedStatement){
        List<MarkDto> markDtoList = new ArrayList<>();
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                markDtoList.add(MarkMapper.mapResultSetToDto(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return markDtoList;
    }

    @Override
    public MarkDto getMarkDtoById(Integer id) {
        MarkDto result = null;
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from marks where id = ?")) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        result = MarkMapper.mapResultSetToDto(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void setMark(MarkDto markDto) {
        String sqlText =
                "INSERT INTO marks (id, id_study, id_student, grade)\n" +
                        "VALUES\n" +
                        "(\n" +
                        "  COALESCE(NULLIF(?, -1),nextval('marks_id_seq')),\n" + // 1 id
                        "?," + // 2 id_study
                        "?," + // 3 id_student
                        "?" + // 4 grade
                        ")\n" +
                        "ON CONFLICT (id)\n" +
                        "   DO  UPDATE\n" +
                        "     SET id_study = EXCLUDED.id_study, id_student = EXCLUDED.id_student, grade = EXCLUDED.grade";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, markDto.getId());
                preparedStatement.setInt(2, markDto.getIdStudy());
                preparedStatement.setInt(3, markDto.getIdStudent());
                preparedStatement.setInt(4, markDto.getGrade());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeMark(MarkDto markDto) {
        String sqlText = "DELETE FROM marks WHERE id = ?";
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlText)) {
                preparedStatement.setInt(1, markDto.getId());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<MarkDto> getMarkListByStudyId(int idStudy) {
        List<MarkDto> markDtoList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from marks WHERE id_study = ?")) {
                preparedStatement.setInt(1, idStudy);
                return getMarkDtoListWithStatement(preparedStatement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return markDtoList;
    }

    @Override
    public List<MarkDto> getMarkListByStudentId(int idStudent) {
        List<MarkDto> markDtoList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from marks WHERE id_student = ?")) {
                preparedStatement.setInt(1, idStudent);
                return getMarkDtoListWithStatement(preparedStatement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return markDtoList;
    }
}
