package jdbc_practice.da;

import jdbc_practice.da.dto.StudyDto;

import java.util.List;

public interface StudyDao {
    public StudyDto getStudyDtoById(Integer id);
    public void setStudy(StudyDto studyDto);
    public void removeStudy(StudyDto groupDto);
    public List<StudyDto> getStudyListByGroupId(int idGroup);
    public List<StudyDto> getStudyList();
}
