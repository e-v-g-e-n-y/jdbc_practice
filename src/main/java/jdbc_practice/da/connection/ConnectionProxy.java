package jdbc_practice.da.connection;

import jdbc_practice.AppRunTimeException;
import jdbc_practice.Log;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;

public class ConnectionProxy implements InvocationHandler {
    private Connection connection;

    public ConnectionProxy(Connection connection) {
        this.connection = connection;
    }

    public void Release() {
        this.connection =null;
    }

    public boolean isReleased() {
        return (connection == null);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (connection == null) {throw  new AppRunTimeException("Соединение закрыто. Для продолжения работы получите новое соединение!");}
        Log.addDebug(method.getName());
        if (method.getName() == "close") {
            Release();
            return null;
        }
        else {
            return method.invoke(connection, args);
        }
    }
}
