package jdbc_practice.da.connection;

import jdbc_practice.AppRunTimeException;
import jdbc_practice.Log;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionManagerJdbc {
    private final int minPollSize = 1;
    private String url;
    private String user;
    private String password;
    private List<Connection> connectionList;
    private List<ConnectionProxy> proxyList;


    public ConnectionManagerJdbc(int poolSize, String url, String user, String password) {
        if (poolSize < minPollSize) { throw new AppRunTimeException("Размер пуля соединений не может быть меньше " + minPollSize); }
        this.url = url;
        this.user = user;
        this.password = password;
        connectionList = new ArrayList<>(poolSize);
        proxyList = new ArrayList<>(poolSize);
        for (int i = 0; i < poolSize; i++) {
            connectionList.add(null);
            proxyList.add(null);
        }
    }

    private Connection buildConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    url, user, password
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
    private Connection/*Proxy*/ buildProxy(int indexConection) {
        ConnectionProxy result;
        Connection proxy;
        Connection connection = connectionList.get(indexConection);
        if (connection == null) {
            connection = buildConnection();
            connectionList.set(indexConection, connection);
        }
        result = new ConnectionProxy(connection);
        proxyList.set(indexConection, result);
        proxy = (Connection) Proxy.newProxyInstance(
                ConnectionProxy.class.getClassLoader(),
                new Class[]{Connection.class},
                result);
        return proxy;
    }
    public Connection getConnection() {
        ConnectionProxy result = null;
        Log.addDbiLog("Получение соелиненния...");
        Log.addDbiLog("proxyList.size=" + proxyList.size());
        for (int i = 0; i < proxyList.size(); i++) {
            Log.addDbiLog("i=" + i);
            if ((proxyList.get(i) == null)||(proxyList.get(i).isReleased())) {
                Log.addDbiLog("buildProxy(i), i=" + i);
                return (Connection)buildProxy(i);
            }
        }
        throw new AppRunTimeException("Все подключения заняты, зайдите позже!");
    }

}

/// старая версия
//public class ConnectionManagerJdbc {
//    public static Connection getConnection() {
//        Connection connection = null;
//        try {
//            connection = DriverManager.getConnection(
//                    "jdbc:postgresql://localhost:5432/DBStudents",
//                    "postgres", "sa");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return connection;
//    }
//}
