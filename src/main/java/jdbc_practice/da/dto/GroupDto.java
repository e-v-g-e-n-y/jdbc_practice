package jdbc_practice.da.dto;

public class GroupDto {
    private int id;
    private String name;
    private int group_graduation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroup_graduation() {
        return group_graduation;
    }

    public void setGroup_graduation(int group_graduation) {
        this.group_graduation = group_graduation;
    }

    @Override
    public String toString() {
        return "GroupDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", group_graduation=" + group_graduation +
                '}';
    }
}
