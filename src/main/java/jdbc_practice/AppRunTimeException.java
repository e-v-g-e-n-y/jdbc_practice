package jdbc_practice;

public class AppRunTimeException extends RuntimeException {
    public AppRunTimeException(String message) {
        super(message);
        Log.addError("AppRunTimeException:" + message);
    }
}
