package jdbc_practice;

public class Log {
    private static boolean enabled = true;
    private static boolean enableFactoriesLog = true;
    private static boolean enableDbiLog = true;
    private static boolean enableModelLog = true;
    private static void Add(String msg) { if (enabled) {System.err.println(msg);}}

    public static void addError(String msg) {Add("*ERR*" + msg);}
    public static void addDebug(String msg) {Add(msg);}
    public static void addFactoriesLog(String msg) { if (enableFactoriesLog) {Add(msg);}}
    public static void addDbiLog(String msg)       { if (enableDbiLog)       {Add(msg);}}
    public static void addModelLog(String msg)     { if (enableModelLog)     {Add(msg);}}
}
