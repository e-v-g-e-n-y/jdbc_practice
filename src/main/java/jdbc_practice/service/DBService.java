package jdbc_practice.service;

import jdbc_practice.AppRunTimeException;
import jdbc_practice.CityFactory;
import jdbc_practice.GroupFactory;
import jdbc_practice.da.*;
import jdbc_practice.da.dto.*;
import jdbc_practice.da.mapper.*;
import jdbc_practice.model.*;

import java.util.ArrayList;
import java.util.List;

public class DBService {
    private StudentDao studentDao;
    private GroupDao groupDao;
    private CityDao cityDao;
    private StudyDao studyDao;
    private MarkDao markDao;
    private GroupFactory groupFactory = new GroupFactory(this);
    private CityFactory cityFactory = new CityFactory(this);


    public DBService(StudentDao studentDao, GroupDao groupDao, CityDao cityDao, StudyDao studyDao, MarkDao markDao) {
        this.studentDao = studentDao;
        this.groupDao = groupDao;
        this.cityDao = cityDao;
        this.studyDao = studyDao;
        this.markDao = markDao;
    }
    public Student getStudent(Integer id) {
        StudentDto studentDto = studentDao.getStudentById(id);
        Student student = StudentMapper.MappDtoToStudent(studentDto, groupFactory, cityFactory);
        return student;
    }
    public void setStudent(Student student) {
        if (student == null) {throw new AppRunTimeException("Нельзя сохранить пустой объект класса Student");}
        // при необходимости сохранение группы и города
        groupFactory.setGroup(student.getGroup());
        cityFactory.setCity(student.getCity());
        //
        StudentDto studentDto = StudentMapper.MappStudentToDto(student);
        studentDao.setStudent(studentDto);
    }
    public void removeStudent(Student student) {
        if (student == null) {throw new AppRunTimeException("Нельзя сохранить пустой объект класса Student");}
        StudentDto studentDto = StudentMapper.MappStudentToDto(student);
        studentDao.removeStudent(studentDto);
    }
    public List<Student> getStudentsByGroup(int idGroup) {
        List<Student> result = new ArrayList<>();
        List<StudentDto> studentDtoList = studentDao.getStudentsByGroupId(idGroup);
        for (StudentDto item : studentDtoList
             ) {
            result.add(StudentMapper.MappDtoToStudent(item, groupFactory, cityFactory));
        }
        return result;
    }
    public List<Student> getStudents() {
        List<Student> result = new ArrayList<>();
        List<StudentDto> studentDtoList = studentDao.getStudents();
        for (StudentDto item : studentDtoList
        ) {
            result.add(StudentMapper.MappDtoToStudent(item, groupFactory, cityFactory));
        }
        return result;
    }
    public Group getGroup(Integer id) {
        GroupDto groupDto = groupDao.getGroupDtoById(id);
        Group group = GroupMapper.MappDtoToGroup(groupDto);
        return group;
    }
    public void setGroup(Group group) {
        if (group == null) {throw new AppRunTimeException("Нельзя сохранить пустой объект класса Group");}
        GroupDto groupDto = GroupMapper.MappGroupToDto(group);
        groupDao.setGroup(groupDto);
    }
    public void removeGroup(Group group) {
        if (group == null) {throw new AppRunTimeException("Нельзя удалить пустой объект класса Group");}
        GroupDto groupDto = GroupMapper.MappGroupToDto(group);
        groupDao.removeGroup(groupDto);
    }
    public City getCity(Integer id) {
        CityDto cityDto = cityDao.getCityDtoById(id);
        City city = CityMapper.MappDtoToCity(cityDto);
        return city;
    }
    public void setCity(City city) {
        if (city == null) {throw new AppRunTimeException("Нельзя сохранить пустой объект класса City");}
        CityDto cityDto = CityMapper.MappCityToDto(city);
        cityDao.setCity(cityDto);
    }
    public void removeCity(City city){
        if (city == null) {throw new AppRunTimeException("Нельзя удалить пустой объект класса City");}
        CityDto cityDto = CityMapper.MappCityToDto(city);
        cityDao.removeCity(cityDto);
    }
    public Study getStudyById(int id) {
        StudyDto studyDto = studyDao.getStudyDtoById(id);
        Study result = StudyMapper.MappDtoToStudy(studyDto);
        return result;
    }
    public List<Study> getStudyListByGroupId(int idGroup) {
        List<Study> result = new ArrayList<>();
        List<StudyDto> studyDtoList = studyDao.getStudyListByGroupId(idGroup);
        for (StudyDto item : studyDtoList) {
            result.add(StudyMapper.MappDtoToStudy(item));
        }
        return result;
    }
    public List<Study> getStudyList() {
        List<Study> result = new ArrayList<>();
        List<StudyDto> studyDtoList = studyDao.getStudyList();
        for (StudyDto item : studyDtoList) {
            result.add(StudyMapper.MappDtoToStudy(item));
        }
        return result;
    }
    public void setStudy(Study study) {
        if (study == null) {throw new AppRunTimeException("Нельзя сохранить пустой объект класса Study");}
        StudyDto studyDto = StudyMapper.MappStudyToDto(study);
        studyDao.setStudy(studyDto);
    }
    public void removeStudy(Study study){
        if (study == null) {throw new AppRunTimeException("Нельзя удалить пустой объект класса Study");}
        StudyDto studyDto = StudyMapper.MappStudyToDto(study);
        studyDao.removeStudy(studyDto);
    }
    public Mark getMarkById(int id) {
        MarkDto markDto = markDao.getMarkDtoById(id);
        Mark result = MarkMapper.MappDtoToMark(markDto);
        return result;
    }
    public List<Mark> getMarkListByStudentId(int idStudent) {
        List<Mark> result = new ArrayList<>();
        List<MarkDto> markDtoList = markDao.getMarkListByStudentId(idStudent);
        for (MarkDto item : markDtoList) {
            result.add(MarkMapper.MappDtoToMark(item));
        }
        return result;
    }
    public List<Mark> getMarkListByStudyId(int idStudy) {
        List<Mark> result = new ArrayList<>();
        List<MarkDto> markDtoList = markDao.getMarkListByStudyId(idStudy);
        for (MarkDto item : markDtoList) {
            result.add(MarkMapper.MappDtoToMark(item));
        }
        return result;
    }

    public void setMark(Mark mark) {
        if (mark == null) {throw new AppRunTimeException("Нельзя сохранить пустой объект класса Mark");}
        MarkDto markDto = MarkMapper.MappMarkToDto(mark);
        markDao.setMark(markDto);
    }
    public void removeMark(Mark mark){
        if (mark == null) {throw new AppRunTimeException("Нельзя удалить пустой объект класса Mark");}
        MarkDto markDto = MarkMapper.MappMarkToDto(mark);
        markDao.removeMark(markDto);
    }

}
