package jdbc_practice;

import jdbc_practice.model.City;
import jdbc_practice.service.DBService;

import java.util.HashMap;
import java.util.Map;

public class CityFactory {
    private Map<Integer, City> cities = new HashMap<>();
    private DBService dbService;

    public CityFactory(DBService dbService) {
        this.dbService = dbService;
    }

    public City getCity(Integer id) {
        City result = cities.get(id);
        if ((result == null) && (dbService != null)){
            result = dbService.getCity(id);
            if (result != null) {
                cities.put(id, result);
                return result;
            }
        }
        return result;
    }
    public void setCity(City city) {
        Log.addFactoriesLog("CityFactory.setCity:start with city=" + city);
        if (city == null) {
            Log.addError("CityFactory.setCity:Попытка сохранить пустой город:" + city);
            return;
        }
        City existingCity = getCity(city.getId());
        if (existingCity != null) {
            // группа с таким id есть, надо сравнить с группой-параметром и при необходимости записать в БД
            Log.addFactoriesLog("CityFactory.setCity: dictionaryObj=" + existingCity);
            if (existingCity.equals(city)) {
                Log.addFactoriesLog("CityFactory.setCity: cities are equal(" + city + "," + existingCity);
                return;
            }
        }
        // такой группы нет в БД
        Log.addFactoriesLog("CityFactory.setCity:adding city");
        dbService.setCity(city);
        Log.addFactoriesLog("CityFactory.setCity:finish with group=" + city);
    }
}
